from setuptools import setup, find_packages
import os

setup(name="sw_challenge",
      version="0.1.0",
      url='',
      license='Proprietary, Orbem GmbH - 2020',
      author='Software team',
      author_email='sw@orbem.ai',
      description='Technical recruting challenge',
      package_dir={"sw_challenge": "src/challenge"},
      packages=['sw_challenge'],
      long_description=open('README.md').read(),
      long_description_content_type='text/markdown',
      install_requires=open('requirements.txt').read(),
      zip_safe=False)
