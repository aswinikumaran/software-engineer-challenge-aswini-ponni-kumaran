# Software Engineers Challenge


## Task 1: Queue processing flow implemented
Priority: High

Description: Complete the producer-consumer example provided in `src/challenge/`, following the best practices you consider relevant.

Deliverables:
- Merge Request including a written description of the implementation when creating the merge request. 
- New unit tests to maximize code coverage.

Resources:
- Example code with the partial implementation.
- Example base of unit tests, in the `tests/` directory.

## Task 2: Code review
Priority: Medium

Description: Review the code in Merge Request provided.

Deliverables:
- Comments on the **only open Merge Request in the repository**.

Resources:
- The **only open Merge Request in the repository**.

## Task 3: What and Why
Priority: Low

Description: Please describe the tools of your choice for Python programming and briefly explain why you chose them:
- IDE
- Linter
- Formatter and style
- CI
- Codebase management
- Code quality analysis
- Testing
- Documentation
- Issue tracker
- Python environment manager

Deliverable:
- Document naming the option of choice and at least one reason for each.

Resources: None

## Task 4: CI pipeline 
Priority: Medium

Description: Our CI pipeline is made out of two steps: unit testing and deployment. However, the first step is not completed and succeeds without running the unit tests. Please make it functional.

Deliverable:
- MR with the changes in the CI code.

Resources:
- GitLab CI file
